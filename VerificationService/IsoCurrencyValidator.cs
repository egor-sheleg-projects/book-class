using System;
using System.Collections;
using System.Diagnostics;
using System.Globalization;
using System.Linq;

namespace VerificationService
{
    /// <summary>
    /// Class for validating currency strings.
    /// </summary>
    public static class IsoCurrencyValidator
    {
        /// <summary>
        /// Determines whether a specified string is a valid ISO currency symbol.
        /// </summary>
        /// <param name="currency">Currency string to check.</param>
        /// <returns>
        /// <see langword="true"/> if <paramref name="currency"/> is a valid ISO currency symbol; <see langword="false"/> otherwise.
        /// </returns>
        /// <exception cref="ArgumentNullException">Thrown if currency is null.</exception>
        public static bool IsValid(string? currency)
        {
            if (currency is null)
            {
                throw new ArgumentNullException(nameof(currency));
            }

            CultureInfo[] cultures = CultureInfo.GetCultures(CultureTypes.SpecificCultures);
            foreach (CultureInfo ci in cultures)
            {
                RegionInfo ri = new RegionInfo(ci.Name);
                if (ri.ISOCurrencySymbol == currency)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
