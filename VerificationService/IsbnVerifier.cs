using System;

namespace VerificationService
{
    /// <summary>
    /// Verifies if the string representation of number is a valid ISBN-10 or ISBN-13 identification number of book.
    /// </summary>
    public static class IsbnVerifier
    {
        /// <summary>
        /// Verifies if the string representation of number is a valid ISBN-10 or ISBN-13 identification number of book.
        /// </summary>
        /// <param name="isbn">The string representation of book's isbn.</param>
        /// <returns>true if number is a valid ISBN-10 or ISBN-13 identification number of book, false otherwise.</returns>
        /// <exception cref="ArgumentNullException">Thrown if isbn is null.</exception>
        public static bool IsValid(string? isbn)
        {
            bool result;

            if (!(isbn is null))
            {
                int count = 0;
                for (int i = 0; i < isbn.Length; i++)
                {
                    if (isbn[i] == '-')
                    {
                        if (i < isbn.Length - 1 && isbn[i + 1] == '-')
                        {
                            return false;
                        }

                        if (i == 1 && isbn[i + 2] == '-')
                        {
                            return false;
                        }

                        count++;
                    }
                }

                if (count < 5)
                {
                    isbn = isbn.Replace("-", string.Empty);
                    switch (isbn.Length)
                    {
                        case 10:
                            result = IsValidISBN10(isbn);
                            break;
                        case 13:
                            result = IsValidISBN13(isbn);
                            break;
                        default:
                            result = false;
                            break;
                    }
                }
                else
                {
                    result = false;
                }

                return result;
            }
            else
            {
                throw new ArgumentNullException(nameof(isbn));
            }
        }

        public static bool IsValidISBN10(string isbn)
        {
            bool result = false;
            int sum = 0;

            for (int i = 0; i < 9; i++)
            {
                sum += (isbn[i] - '0') * (i + 1);
            }

            sum += (isbn[9] == 'X' ? 10 : (isbn[9] - '0')) * 10;

            result = sum % 11 == 0;

            return result;
        }

        public static bool IsValidISBN13(string isbn)
        {
            bool result = false;
            int sum = 0;

            for (int i = 0; i < 13; i++)
            {
                if (i % 2 == 0)
                {
                    sum += (isbn[i] - '0') * 1;
                }
                else
                {
                    sum += (isbn[i] - '0') * 3;
                }
            }

            result = sum % 10 == 0;

            return result;
        }
    }
}
