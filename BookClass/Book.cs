﻿using System;
using System.Diagnostics;
using System.Globalization;
using VerificationService;

namespace BookClass
{
    /// <summary>
    /// Represents the book as a type of publication.
    /// </summary>
    public sealed class Book
    {
        private int totalPages;
        private bool published;
        private DateTime datePublished;

        /// <summary>
        /// Initializes a new instance of the <see cref="Book"/> class.
        /// </summary>
        /// <param name="author">Autor of the book.</param>
        /// <param name="title">Title of the book.</param>
        /// <param name="publisher">Publisher of the book.</param>
        /// <exception cref="ArgumentNullException">Throw when author or title or publisher is null.</exception>
        public Book(string author, string title, string publisher)
            : this(author, title, publisher, string.Empty)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Book"/> class.
        /// </summary>
        /// <param name="author">Autor of the book.</param>
        /// <param name="title">Title of the book.</param>
        /// <param name="publisher">Publisher of the book.</param>
        /// <param name="isbn">International Standard Book Number.</param>
        /// <exception cref="ArgumentNullException">Throw when author or title or publisher or ISBN is null.</exception>
        public Book(string author, string title, string publisher, string isbn)
        {
            if (author is null || title is null || publisher is null || isbn is null)
            {
                throw new ArgumentNullException(nameof(title), "Something is null");
            }

            this.Author = author;
            this.Title = title;
            this.Publisher = publisher;
            if (IsbnVerifier.IsValid(isbn))
            {
                this.ISBN = isbn;
            }
        }

        /// <summary>
        /// Gets author of the book.
        /// </summary>
        public string Author { get; }

        /// <summary>
        /// Gets title of the book.
        /// </summary>
        public string Title { get; }

        /// <summary>
        /// Gets publisher of the book.
        /// </summary>
        public string Publisher { get; }

        /// <summary>
        /// Gets or sets total pages in the book.
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException">Throw when Pages less or equal zero.</exception>
        public int Pages
        {
            get
            {
                return this.totalPages;
            }

            set
            {
                if (value <= 0)
                {
                    throw new ArgumentOutOfRangeException(nameof(this.Pages));
                }

                this.totalPages = value;
            }
        }

        /// <summary>
        /// Gets International Standard Book Number.
        /// </summary>
        public string ISBN { get; } = string.Empty;

        /// <summary>
        /// Gets price.
        /// </summary>
        public decimal Price { get; private set; }

        /// <summary>
        /// Gets currency.
        /// </summary>
        public string Currency { get; private set; } = string.Empty;

        /// <summary>
        /// Publishes the book if it has not yet been published.
        /// </summary>
        /// <param name="dateTime">Date of publish.</param>
        public void Publish(DateTime dateTime)
        {
            this.published = true;
            this.datePublished = dateTime;
        }

        /// <summary>
        /// String representation of book.
        /// </summary>
        /// <returns>Representation of book.</returns>
        public override string ToString()
        {
            return $"" + this.Title + " by " + this.Author;
        }

        /// <summary>
        /// Gets a information about time of publish.
        /// </summary>
        /// <returns>The string "NYP" if book not published, and the value of the datePublished if it is published.</returns>
        #pragma warning disable CA1024
        public string GetPublicationDate()
        {
            if (this.published)
            {
                string db;
                if (this.datePublished.Day < 10)
                {
                    if (this.datePublished.Month < 10)
                    {
                        db = "0" + this.datePublished.Month + "/0" + this.datePublished.Day + "/" + this.datePublished.Year;
                    }
                    else
                    {
                        db = this.datePublished.Month + "/0" + this.datePublished.Day + "/" + this.datePublished.Year;
                    }

                    return db;
                }
                else
                {
                    if (this.datePublished.Month < 10)
                    {
                        db = "0" + this.datePublished.Month + "/" + this.datePublished.Day + "/" + this.datePublished.Year;
                    }
                    else
                    {
                        db = this.datePublished.Month + "/" + this.datePublished.Day + "/" + this.datePublished.Year;
                    }

                    return db;
                }
            }
            else
            {
                return "NYP";
            }
        }

        /// <summary>
        /// Sets the prise and currency of the book.
        /// </summary>
        /// <param name="price">Price of book.</param>
        /// <param name="currency">Currency of book.</param>
        /// <exception cref="ArgumentException">Throw when Price less than zero or currency is invalid.</exception>
        /// <exception cref="ArgumentNullException">Throw when currency is null.</exception>
        public void SetPrice(decimal price, string currency)
        {
            if (currency is null)
            {
                throw new ArgumentNullException(nameof(currency));
            }

            if (IsoCurrencyValidator.IsValid(currency) && price >= 0)
            {
                this.Price = price;
                this.Currency = currency;
            }
            else
            {
                throw new ArgumentException("Price less than zero or currency is invalid.");
            }
        }
    }
}
